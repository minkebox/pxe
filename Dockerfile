FROM alpine:edge

RUN apk --no-cache add dnsmasq ;\
    rm -f /etc/dnsmasq.conf

COPY root/ /

EXPOSE 67/udp

ENTRYPOINT ["/startup.sh"]

