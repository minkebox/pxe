#! /bin/sh

trap "echo 'Terminating'; killall sleep dnsmasq; exit" TERM

DEFAULT_BRDCAST=$(ip addr show dev ${__DEFAULT_INTERFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | tail -1)

cat > /etc/dnsmasq.conf <<__EOF__
user=root
port=0
dhcp-range=${DEFAULT_BRDCAST},proxy
__EOF__

id=1
cat /mappings | while read line ; do
  mac=$(echo $line | cut -d"#" -f 1)
  ip=$(echo $line | cut -d"#" -f 2)
  csa=$(echo $line | cut -d"#" -f 3)
  menu=$(echo $line | cut -d"#" -f 4)
  filename=$(echo $line | cut -d"#" -f 5)
  cat >> /etc/dnsmasq.conf <<__EOF__
dhcp-host=${mac},set:h${id}
pxe-service=tag:h${id},${csa},"${menu}"
dhcp-boot=tag:h${id},${filename},,${ip}
__EOF__
  id=$((${id} + 1))
done

dnsmasq

sleep 2147483647d &
wait "$!"
